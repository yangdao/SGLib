﻿using SGLisp;

namespace SGLib.AITree
{
    class AIFuncNode : AINode
    {
        ListNode mFuncNode;
        public override void Load(ListNode astNode)
        {
            this.mNodeState = AINodeState.Wait;
            this.mFuncNode = astNode;
        }

        public override void OnEnter(Context context)
        {
            ASTNode RetNode = RT.Evaluate(this.mFuncNode,context);
            if (RetNode != null)
            {
                StringNode strNode = (StringNode)RetNode;
                this.mNodeState = AINodeState.Error;
                return;
            }

            this.mNodeState = AINodeState.Finish;
        }

        public override void OnExit()
        {
         
        }
    }
}
