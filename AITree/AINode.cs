﻿using System;
using System.Collections.Generic;
using SGLisp;

namespace SGLib.AITree
{
    public enum AINodeState
    {
        Wait,
        Run,
        Finish,
        Error
    }
    public class AINode
    {
        protected AITree mTree;
        protected AINode Parent;
        protected List<AINode> mChildrens;

        protected  AINodeType mNodeType;
        public AINodeType NodeType { get { return mNodeType; } }

        protected AINodeState mNodeState;
        public AINodeState NodeState {get { return mNodeState; } }

        public void Init(AITree tree,AINode parent)
        {
            this.mTree = tree;
            Parent = parent;
        } 

        public virtual void Load(ListNode astNode)
        {

        }
        public virtual void Update(Context context,float dt)
        {

        }
        public virtual void OnExec() { }
        public virtual void OnEnter(Context context) { }
        public virtual void OnExit() { }
    }
}
