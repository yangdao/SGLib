﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGLisp;

namespace SGLib.AITree
{
     public class AITreeContext
    {
        public Context Context;
        Dictionary<string, Func<AINode>> mRegisterNodes = new Dictionary<string, Func<AINode>>();
        public AITreeContext(Context parentContext)
        {
            Context = new Context(parentContext);
        }

        public void RegisterNode(string nodeName,Func<AINode> node)
        {
            mRegisterNodes.Add(nodeName,node);
        }

        public AINode GetRegisterNode(string funcName)
        {
            if (mRegisterNodes.ContainsKey(funcName))
            {
               AINode newFuncNode  =  mRegisterNodes[funcName]();
                return newFuncNode;
            }
            return null;
        }
    }
}
