﻿using System;
using System.Collections.Generic;
using System.Text;
namespace SGLib.Net
{
    public interface IPosList<T> : IList<T>
    {
        int Position { get; set; }
    }


    public class PosList<T> : List<T>, IPosList<T>
    {

        public int Position { get; set; }
    }

}