﻿namespace SGLib.Net
{
    public interface ICodec
    {
        byte[] Code(params object[] datas);
        object Decode(params object[] datas);
        int GetPackageSize(params object[] datas);
    }

    public interface IPackage
    {

    }
}
