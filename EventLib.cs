﻿using System.Collections.Generic;

namespace SGLib
{
    public class EventContainer
    {

        public delegate void HandleFunc(string eventName,params object[] args);
        Dictionary<string, List<HandleFunc >> mRegisterHandlers =  new Dictionary<string, List<HandleFunc>>();


        public void AddEventHandler(string handlerName,HandleFunc func)
        {
            if (!mRegisterHandlers.ContainsKey(handlerName))
            {
                mRegisterHandlers[handlerName] = new List<HandleFunc>();
            }
            mRegisterHandlers[handlerName].Add(func);
        }

        public void RemoveEventHandler(string handlerName,HandleFunc func)
        {
            if (mRegisterHandlers.ContainsKey(handlerName))
            {
                mRegisterHandlers[handlerName].Remove(func);
            }
        }

        public void CallEvent(string handlerName,params object[] args)
        {
            if (mRegisterHandlers.ContainsKey(handlerName))
            {
                foreach (HandleFunc fn in mRegisterHandlers[handlerName])
                {
                    fn(handlerName, args);
                }
            }
        }
   
    } 

}
