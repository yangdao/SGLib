﻿using System.Collections;
using System.Collections.Generic;
namespace SGLisp
{
    public class ASTNode
    {
        public enum ASTNodeType
        {
            BoolNode,
            CharacterNode,
            CommentNode,
            DerefNode,
            KeywordNode,
            ListNode,
            MapNode,
            MetadataNode,
            NewlineNode,
            NilNode,
            NumberNode,
            SymbolNode,
            QuoteNode,
            StringNode,
            SyntaxQuoteNode,
            UnquoteNode,
            UnquoteSpliceNode,
            VectorNode,
            FnLiteralNode,
            IgnoreFormNode,
            RegexNode,
            SetNode,
            VarQuoteNode,
            TagNode,
            FunctionNode,
            ObjectNode,
        }

        public ASTNodeType NodeType; 
 
        public ASTPos Position;

        public virtual ASTNode CopyNode() { return null;}
        public virtual string String() { return ""; }

        public static int countSemantic(List<ASTNode> nodes)
        {
            int count = 0;
            for (int i = 0; i < nodes.Count; i++)
            {
                if (isSemantic(nodes[i]))
                {
                    count++;
                }
            }
            return count;
        }

        public static bool isSemantic(ASTNode n)
        {
            if (n == null) 
            {
                return false;
            }
            if (n.GetType() == typeof(CommentNode) || n.GetType() == typeof(NewlineNode))
            {
                return false;
            }
            return true;
        }
    }


    public class BoolNode : ASTNode
    {
        public bool Val;
        public BoolNode() { this.NodeType = ASTNodeType.BoolNode; }
        public BoolNode(bool b) { this.Val = b; this.NodeType = ASTNodeType.BoolNode; }
        public override string String()
        {
            if (this.Val)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
    }

    public class CharacterNode : ASTNode
    {
        public char Val;
        public string Text;

        public CharacterNode() { this.NodeType = ASTNodeType.CharacterNode; }
        public override string String()
        {
            return "char(" + Val.ToString() + ")";
        }
    }

    public class CommentNode : ASTNode
    {
        public string Text;
        public CommentNode() { this.NodeType = ASTNodeType.CommentNode; }
        public override string String()
        {
            return "comment(" + this.Text + ")";
        }
    }

    public class DerefNode : ASTNode
    {
        public ASTNode Node;
        public DerefNode() { this.NodeType = ASTNodeType.DerefNode; }
        public override string String()
        {
            return "deref";
        }
    }

    public class KeywordNode : ASTNode
    {
        public string Val;

        public KeywordNode() { this.NodeType = ASTNodeType.KeywordNode; }
        public KeywordNode(string val) { this.Val = val; this.NodeType = ASTNodeType.KeywordNode; }
        public override string String()
        {
            return "keyword(" + Val + ")";
        }

        public override bool Equals(object obj)
        {
            if (obj is KeywordNode&& ((KeywordNode)obj).Val ==this.Val)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.Val.GetHashCode();
        }
    }

    public class ListNode : ASTNode
    {
        public LinkedList<ASTNode> nodes;
        public bool IsQuote = false;

        public ListNode() { this.NodeType = ASTNodeType.ListNode; }
        public override string String()
        {
            return "list(length=" + nodes.Count + ")";
        }
    }

    public class MapNode : ASTNode
    {
        public Hashtable Values; 

        public MapNode() { this.NodeType = ASTNodeType.MapNode; }
        public override string String()
        {
            return "map(length=" + Values.Count + ")";
        }
        
    }

    public class MetadataNode : ASTNode
    {
        ASTNode node;
        public MetadataNode() { this.NodeType = ASTNodeType.MetadataNode; }
        public override string String()
        {
            return "metadata";
        }
    }

    public class NewlineNode : ASTNode
    {
        public NewlineNode() { this.NodeType = ASTNodeType.NewlineNode; }
        public override string String()
        {
            return "newline";
        }
    }

    public class NilNode : ASTNode
    {
        public NilNode() { this.NodeType = ASTNodeType.NilNode; }
        public override string String()
        {
            return "nil";
        }
    }

    public class NumberNode : ASTNode
    {
        public object NumberVal; 
        public enum NumberType 
        {
            INT,
            FLOAT,
        }
        public NumberType Type; 
        public NumberNode() { this.NodeType = ASTNodeType.NumberNode; }

        public NumberNode(object number, NumberType type) 
        {
            this.NumberVal = number;
            this.Type = type;
            this.NodeType = ASTNodeType.NumberNode;
        }
        public NumberNode(object number)
        {
            this.NumberVal = number;
           
            this.NodeType = ASTNodeType.NumberNode;
            if (number is float)
            {
                this.Type = NumberType.FLOAT;
            }
            else if (number is int)
            {
                this.Type = NumberType.INT;
            }
            else
            {
                throw new System.Exception("错误的数字类型");
            }
        }
        public override string ToString()
        {
            return "num(" + this.NumberVal.ToString() + ")";
        }
    }

    public class SymbolNode : ASTNode
    {
        public bool IsQuote = false;
        public string Val;
        public SymbolNode() { this.NodeType = ASTNodeType.SymbolNode; }
        public SymbolNode(string symName) { this.NodeType = ASTNodeType.SymbolNode;this.Val = symName; }

        public override ASTNode CopyNode()
        {
            return new SymbolNode(Val);
        }

        public override string String()
        {
            return "sym(" + this.Val + ")";
        }
    }

    public class FunctionNode : ASTNode, IFunction
    {
        public delegate ASTNode ASTFunc(List<ASTNode> args,Context context);
        public ASTFunc mFunc;
        public enum FuncType
        {
            Core,
            SGLisp
        }
        public FuncType FnType;

        public ListNode lstNode;
        public VectorNode vecArgNode; 

        public FunctionNode() { this.NodeType = ASTNodeType.FunctionNode; }
        public FunctionNode(ASTFunc fn) { this.NodeType = ASTNodeType.FunctionNode; this.mFunc = fn; this.FnType = FuncType.Core; }

        public  ASTNode Exec(List<ASTNode> args, Context context)
        {
            //Core Func
            if (this.FnType == FuncType.Core)
            {
                
                return mFunc(args, context);
            }

            //Clojure Func
            Context FuncContext = new Context(context);
            if (args != null)
            {
               // List<ASTNode> newArgs = CoreFunction.EvalArgs(args, context);
                if (vecArgNode != null)
                {
                    for (int i = 0; i < vecArgNode.nodes.Length; i++)
                    {
                        string SymName = ((SymbolNode)vecArgNode.nodes[i]).Val;

                        FuncContext.Define(SymName, args[i]);
                    }
                }
                else
                {
                    for (int i = 0; i < args.Count; i++)
                    {
                        string SymName = i == 0 ? "%" : "%" + (i + 1).ToString();
                        FuncContext.Define(SymName, args[i]);
                    }
                }
            }
            
            ASTNode retNode = RT.Evaluate(lstNode,FuncContext);
            return retNode;
        }
        
    }
    public class ObjectNode : ASTNode 
    {
        public object Val;
        public ObjectNode() { this.NodeType = ASTNodeType.ObjectNode; }

        public ObjectNode(object val) { this.NodeType = ASTNodeType.ObjectNode; this.Val = val; }

    }

    public class QuoteNode : ASTNode
    {
        public ASTNode Node;

        public QuoteNode() { this.NodeType = ASTNodeType.QuoteNode; }
        public QuoteNode(ASTNode node) { this.NodeType = ASTNodeType.QuoteNode; this.Node = node; }
        public override string String()
        {
            return "quote";
        }
       
    }

    public class StringNode : ASTNode
    {
        public string Val;
        public StringNode(string strVal) { this.Val = strVal; this.NodeType = ASTNodeType.StringNode; }
        public StringNode() { this.NodeType = ASTNodeType.StringNode; }
        public override string ToString()
        {
            return "string(" + Val + ")";
        }

    }

    public class SyntaxQuoteNode : ASTNode
    {
        ASTNode node;

        public SyntaxQuoteNode() { this.NodeType = ASTNodeType.SyntaxQuoteNode; }
        public override string String()
        {
            return "syntax quote";
        }
       
    }

    public class UnquoteNode : ASTNode
    {
        ASTNode node;

        public UnquoteNode() { this.NodeType = ASTNodeType.UnquoteNode; }
        public override string String()
        {
            return "unquote";
        }

        
    }

    public class UnquoteSpliceNode : ASTNode
    {
        ASTNode node;

        public UnquoteSpliceNode() 
        {
            this.NodeType = ASTNodeType.UnquoteSpliceNode;
        }
        public override string String()
        {
            return "unquote splice";
        }

        
    }

    public class VectorNode : ASTNode
    {
        public ASTNode[] nodes;
        public VectorNode() { this.NodeType = ASTNodeType.VectorNode; }
        public override string String()
        {
            return "vector(length=" + nodes.Length + ")";
        }
    }

    public class FnLiteralNode : ASTNode
    {
        public List<ASTNode> nodes;

        public FnLiteralNode() { this.NodeType = ASTNodeType.FnLiteralNode; }
        public override string String()
        {
            return "lambda(length=" + countSemantic(this.nodes) + ")";
        }
        
    }

    public class IgnoreFormNode : ASTNode
    {
        ASTNode node;

        public IgnoreFormNode() { this.NodeType = ASTNodeType.IgnoreFormNode; }
        public override string String()
        {
            return "ignore";
        }

       
    }

    public class RegexNode : ASTNode
    {
        public string Val;

        public RegexNode() { this.NodeType = ASTNodeType.RegexNode; }
        public override string String()
        {
            return "regex(" + this.Val + ")";
        }
    }

    public class SetNode : ASTNode
    {
        public SetNode() { this.NodeType = ASTNodeType.SetNode; }
        public override string String()
        {
            return "set()";
        }
        
    }

    public class VarQuoteNode : ASTNode
    {
        public string Val;

        public VarQuoteNode() { this.NodeType = ASTNodeType.VarQuoteNode; }
        public override string String()
        {
            return "varquote(" + Val.ToString() + ")";
        }
    }

    public class TagNode : ASTNode
    {
        public string Val;

        public TagNode() { this.NodeType = ASTNodeType.TagNode; }
        public override string String()
        {
            return "tag(" + Val + ")";
        }
    }


}