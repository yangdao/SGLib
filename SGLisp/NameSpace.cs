﻿namespace SGLisp
{
    public class NameSpace
    {
        public string NameSpaceName;
        public Context NameSpaceContext;
        public NameSpace(string namespaceName) 
        {
            this.NameSpaceName = namespaceName;
            this.NameSpaceContext = new Context(RT.MainContext);
        }
    } 
}