﻿using System.Collections.Generic;
namespace SGLisp
{
    /// <summary>
    /// 可以并行的函数队列
    /// </summary>
    public class FuncSeqConcurrent
    {
        //原始的FuncSeq
        FuncSeq RawFuncSeq;
        List<FuncSeq> FuncSeqPool;

        public FuncSeqConcurrent(FuncSeq funcSeq)
        {
            this.RawFuncSeq = funcSeq;
            this.FuncSeqPool = new List<FuncSeq>();
        }

        //获取一个FuncSeq实例
        public FuncSeq GetSeqFuncInst()
        {
            for (int i=0;i<this.FuncSeqPool.Count;i++)
            {
                if (this.FuncSeqPool[i].IsRun == false)
                {
                    return this.FuncSeqPool[i];
                }
            }
            FuncSeq newFuncSeq = this.RawFuncSeq.Clone();
            this.FuncSeqPool.Add(newFuncSeq);
            return newFuncSeq;
        }

        public void Run()
        {
            this.GetSeqFuncInst().Run();
        }

        public void Update(Context context)
        {
            for (int i=0;i<this.FuncSeqPool.Count;i++)
            {
                if (this.FuncSeqPool[i].IsRun)
                {
                    this.FuncSeqPool[i].Update(context);
                }
            }
        }
    }
}
