﻿namespace SGLisp
{
    public enum FuncSeqType
    {
        Block,
        NonBlock,
    }
    public interface IFuncSeqNode
    {
        void Exec(Context context);
        void Update(Context context);
        bool IsExec { get; set; }
        FuncSeqType FuncSeqType { get; set; }
        IFuncSeqNode Clone();
    }

    public interface IFuncSeqBlockNode : IFuncSeqNode
    {
        void Exit();
        bool IsFinish { get; set; }
        bool IsWait { get; set; }

        void Stop();
       
    }
}