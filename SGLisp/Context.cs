﻿using System.Collections.Generic;
namespace SGLisp
{

    public class Context
    {
        Dictionary<string, ASTNode> Vars;
        protected Context Parent;

        public Context() 
        {
            Vars = new Dictionary<string, ASTNode>();
        }

        public Context Copy()
        {
            Context copyContext = new Context();
            copyContext.Parent = this.Parent;
            copyContext.Vars = new Dictionary<string, ASTNode>(this.Vars);
            return copyContext;
        }

        public Context(Context parent) 
        {
            this.Vars = new Dictionary<string, ASTNode>();
            this.Parent = parent;
        }

        public ASTNode Find(string symbolName) 
        {
            if (this.Vars.ContainsKey(symbolName))
            {
                return this.Vars[symbolName];
            }
            else
            {
                if (Parent != null) 
                {
                  return  Parent.Find(symbolName);
                }
            }
            return null;
        }

        public void Define(string symbolName, ASTNode var) 
        {
            if (this.Vars.ContainsKey(symbolName))
            {
                this.Vars[symbolName] = var;
            }
            else 
            {
                this.Vars.Add(symbolName, var);
            }
        }


      
     
    }



}